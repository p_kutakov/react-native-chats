import React from 'react';
import { Provider } from 'react-redux';
import MainComponent from './src/components/main';
import store from './src/store/store';
import './ReactotronConfig';

const App =()=>{
    return(
        <Provider store={store}>
            <MainComponent/>
        </Provider>
    )
}

export default App