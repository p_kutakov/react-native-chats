import React from 'react';
import { View } from 'react-native';
import { Icon, SearchBar } from 'react-native-elements';
import { styles } from '../styles';

export default class SearchBarComponent extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      soundState: 'notifications',
    }
  }

  getInputValue = (value) => {
    console.log(value);
  }

  changeSoundState = () => {
    const value = this.state.soundState === 'notifications' ? 'notifications-off' : 'notifications';
    this.setState((prevState) => {
      return { ...prevState, soundState: value };
    });
  }

  render() {
    const { soundState } = this.state;
    return (
      <View style={styles.searchBarWrapper}>
        <Icon
          onPress={this.props.actionDrawler}
          containerStyle={styles.searchBarIcon}
          name='sort'/>
        <SearchBar
          onFocus={this.props.changeBehavior}
          containerStyle={styles.searchBarContainer}
          inputStyle={styles.searchBarInput}
          round
          lightTheme
          onChangeText={this.getInputValue}
          onClearText={this.getInputValue}
          placeholder='Search ...'/>
        <Icon
          onPress={this.changeSoundState}
          containerStyle={styles.searchBarIcon}
          name={soundState}/>
      </View>
    )
  }
}