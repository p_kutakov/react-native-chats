import React from 'react';
import { View, Dimensions } from 'react-native';
import { SocialIcon } from 'react-native-elements';
import ReactNativeComponentTree from 'react-native/Libraries/Renderer/shims/ReactNativeComponentTree';
import { styles } from '../styles';

export default class SocialIconsComponent extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      twitter: false,
      instagram: false,
      youtube: false,
      facebook: false,
    };
  }
  toggleIcon = (value, key) => {
    return (previousState) => {
      return { ...previousState, [key]: !value };
    };
  };

  render() {
    const {
      twitter,
      instagram,
      youtube,
      facebook,
    } = this.state;
    const { height } = Dimensions.get('window');
    const iconSize = height > 600 ? 35 : 30;
    return (
      <View style={styles.socialIconsContainer}>
        <SocialIcon
          light={twitter}
          onPress={() => {
            this.setState(this.toggleIcon(twitter, 'twitter'));
          }}
          type='twitter'
          iconSize={iconSize}
        />
        <SocialIcon
          light={instagram}
          type='instagram'
          onPress={() => {
            this.setState(this.toggleIcon(instagram, 'instagram'));
          }}
          iconSize={iconSize}
        />
        <SocialIcon
          light={youtube}
          type='youtube'
          iconSize={iconSize}
          onPress={() => {
            this.setState(this.toggleIcon(youtube, 'youtube'));
          }}
        />
        <SocialIcon
          light={facebook}
          type='facebook'
          iconSize={iconSize}
          onPress={() => {
            this.setState(this.toggleIcon(facebook, 'facebook'));
          }}
        />
      </View>
    );
  }
}
