import React from 'react';
import { View, Text } from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { styles } from '../styles';

export class SliderComponent extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      value: [0, 100],
    };
  }

  multiSliderValuesChange = (values) => {
    this.setState({
      value: values,
    });
  }

  render() {
    const { value } = this.state
    return (
      <View style={styles.sliderBlockContainer}>
        <View style={styles.sliderValuesWrapper}>
          <Text>{value[0]}</Text>
          <Text>{value[1]}</Text>
        </View>
        <View style={styles.multiSliderWrapper}>
          <MultiSlider
            containerStyle={styles.multiSlider}
            trackStyle={{ backgroundColor: '#979797' }}
            selectedStyle={{ backgroundColor: '#979797' }}
            markerStyle={{
              backgroundColor: 'black',
              borderColor: 'black',
              height: 16,
              width: 16,
            }}
            values={[value[0], value[1]]}
            sliderLength={280}
            onValuesChange={this.multiSliderValuesChange}
            min={0}
            max={100}
            step={1}
            allowOverlap
            snapped
          />
        </View>
      </View>
    )
  }
}


export default SliderComponent

