import React from 'react';
import { View, Keyboard, Platform, Text } from 'react-native';
import { Button as StyledButton, Text as StyledText } from 'react-native-elements';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import SliderComponent  from './SliderComponent';
import TextAreaComponent from './TextAreaComponent';
import SocialIconsComponent from './SocialIconsComponent';
import SearchBarComponent from './SearchBarComponent';
import { styles } from '../styles';


export default class PairFilterScreen extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      scrollEnabled: false,
    }
  }

  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = () => {
    console.log('Keyboard Shown');
  }

  _keyboardDidHide = () => {
    this.setState({ scrollEnabled: false })
  }
  getPossiblePairs = () => {
    const { navigate } = this.props.navigation;
    navigate('ContentCreators', { name: 'Content Creators' })
  }
  changeBehavior = () => {
    this.setState({ scrollEnabled: true })
  }

  render() {
    const { actionDrawler } = this.props.screenProps;
    const { scrollEnabled } = this.state;
    return (
      <KeyboardAwareScrollView
        style={styles.insideContainerWrapperIOS}
        extraHeight={180}
        scrollEnabled={scrollEnabled}
        enableOnAndroid={true}
      >
        <View style={{ backgroundColor: "#fafafa" }}>

          <SearchBarComponent
            changeBehavior={this.changeBehavior}
            actionDrawler={actionDrawler}
          />

          <View>
            <StyledText style={styles.smallTitle}>Platform</StyledText>
          </View>

          <SocialIconsComponent />

          <View>
            <StyledText style={styles.smallTitle}>Price</StyledText>
          </View>

          <SliderComponent
            ref={element => {
              this.priceSlider = element;
            }}
          />

          <View>
            <StyledText style={styles.smallTitle}>Exposure</StyledText>
          </View>

          <SliderComponent
            ref={element => {
              this.exposureSlider = element;
            }}
          />

          <View>
            <StyledText style={styles.smallTitle}>Category</StyledText>
          </View>

          <TextAreaComponent
            changeBehavior={this.changeBehavior}
            ref={element => this.textArea = element}
          />

          <StyledButton
            backgroundColor='#9013fe'
            containerViewStyle={styles.pairButton}
            borderRadius={50}
            raised
            rightIcon={{ name: 'done' }}
            title='Pair'
            onPress={this.getPossiblePairs}
          />
          { Platform.OS === 'android' ? <View style={styles.spacer}/> : null}
        </View>
      </KeyboardAwareScrollView>
    )
  }
}


