import React from 'react';
import { View, TextInput } from 'react-native';
import { styles } from '../styles';

export class TextAreaComponent extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      value: '',
    };
  }

  render() {
    return (
      <View style={styles.textAreaContainer}>
        <TextInput
          onFocus={this.props.changeBehavior}
          style={styles.textArea}
          editable={true}
          keyboardType='default'
          returnKeyType='done'
          placeholder='Type Keywords here...'
          placeholderTextColor='#5f6d74'
          onChangeText={(value) => {
            this.setState({ value: value });
          }}
          underlineColorAndroid='rgba(0,0,0,0)'
        />
      </View>
    );
  }
}

export default TextAreaComponent