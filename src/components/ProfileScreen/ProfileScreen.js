import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ActivityIndicator,
  AsyncStorage,
  Image,
  PixelRatio,
} from 'react-native';
import Auth0 from 'react-native-auth0';
import { Button as StyledButton, Icon } from 'react-native-elements';
import axios from 'axios';
import ImagePicker from 'react-native-image-picker';
import ChangeUserData  from './ChangeUserData';
const auth0 = new Auth0({ domain: 'https://dunice.auth0.com', clientId: '79s8TP27Lnr3ALxJAOLqdzxdYYjAo7tm' });


export default class ProfileScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      credentials: {},
      avatar_link: null,
      email: '',
      first_name: '',
      last_name: '',
      bio: '',
      location: '',
      about: '',
      change: false,
      isTimePassed: false,
      editable: false,
      error: null,
      image: null,
      avatarChanged: false,
    };
  }

  selectPhotoTapped() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };
        this.setState({
          avatar_link: response.uri,
          image: response,
          avatarChanged: true,
        });
      }
    });
  }


  componentDidMount() {
    this.getUserData();
  }

  getUserData = async () => {
    try {
      const token = await AsyncStorage.getItem('token');
      const response = await axios({
        method: 'get',
        url: 'http://ec2-54-83-188-86.compute-1.amazonaws.com/profile/me/',
        headers: {
          'x-access-token': token,
        },
      });
      if (response && token !== null) {
        const { avatar_link, email, first_name, last_name } = response.data.data
        this.setState(prevState => {
          return {
            ...prevState,
            first_name: first_name,
            last_name: last_name,
            email: email,
            avatar_link: avatar_link,
            isTimePassed: true,
          };
        });
      }
    } catch (error) {
      if (error.response) {
        if (error.response.data.info === 'TOKEN_EXPIRED') {
          const refresh = await AsyncStorage.getItem('refresh');
          this.props.screenProps.refreshTokens(refresh, this.getUserData);
        }
      } else if (error.request) {
        console.log(error.request);
      } else {
        console.log('Error', error.message);
      }
      console.log(error.config);
      this.setState((prevState) => {
        return { ...prevState, isTimePassed: true };
      })
    }
  };

  getSocialLinks = () => {
    auth0
      .webAuth
      .authorize({ scope: 'openid email', audience: 'https://dunice.auth0.com/userinfo' })
      .then(credentials => console.log(credentials))
      .catch(error => console.log(error));
  }
  changeUserData = () => {
    this.setState((prevState) => {
      return {
        ...prevState, editable: !this.state.editable,
      };
    });
  };
  changeCreds = (value) => {
    this.setState((prevState) => {
      return {
        ...prevState, first_name: value.first_name, last_name: value.last_name, editable: false
      };
    });
  };

  uploadAvatar = async () => {
    this.setState(prevState => {
      return { ...prevState, isTimePassed: false }
    })
    const { image } = this.state;
    const data = new FormData();
    const fileName = image.fileName ? image.fileName : image.uri.substr(-7);
    data.append('avatar', { uri: image.uri, type: 'image/jpeg', name: fileName })

    try {
      const token = await AsyncStorage.getItem('token');
      const refresh = await AsyncStorage.getItem('refresh');
      const response = await axios({
        method: 'post',
        url: 'http://ec2-54-83-188-86.compute-1.amazonaws.com/upload/avatar/',
        headers: {
          'x-access-token': token,
          'x-refresh-token': refresh,
        },
        data: data,
      })
      this.setState((prevState) => {
        return { ...prevState, avatarChanged: false, isTimePassed: true }
      })

    }
    catch (err) {
      this.setState((prevState) => {
        return { ...prevState, avatarChanged: false, isTimePassed: true }
      })
      console.log(err);
    }
  }

  render() {
    const { avatar_link, first_name, last_name, editable, isTimePassed, avatarChanged } = this.state;
    const { actionDrawler } = this.props.screenProps;
    const { navigate } = this.props.navigation;
    if (!isTimePassed) {
      return (
        <View style={styles.container}>
          <ActivityIndicator
            size="large"
            color='#040404'
          />
        </View>
      );
    }
    if (editable) {
      return (
        <ChangeUserData
          saveUserData={this.saveUserData}
          changeUserData={this.changeUserData}
          first_name={this.state.first_name}
          last_name={this.state.last_name}
          email={this.state.email}
          changeCreds={this.changeCreds}
        />
      )
    }
    return (
      <View sytle={styles.container}>
        <View style={styles.searchBarWrapper}>
          <Icon
            onPress={actionDrawler}
            containerStyle={{ alignSelf: 'flex-start', marginLeft: 20, height: 40 }}
            name='sort'/>
        </View>

        <TouchableOpacity style={{ alignSelf: 'center' }} onPress={this.selectPhotoTapped.bind(this)}>
          <View style={[styles.avatar, styles.avatarContainer, { marginBottom: 20 }]}>
            { avatar_link === null ? <Text>Select a Photo</Text> :
              <Image style={styles.avatar} source={{ uri: avatar_link }}/>
            }
          </View>
        </TouchableOpacity>

        <View>
          <Text style={{ textAlign: 'center' }}>
            {first_name}
          </Text>
          <Text style={{ textAlign: 'center' }}>
            {last_name}
          </Text>
        </View>

        <StyledButton
          backgroundColor='#9013fe'
          containerViewStyle={styles.pairButton}
          borderRadius={50}
          raised
          title='Edit profile'
          onPress={this.changeUserData}
        />

        <StyledButton
          backgroundColor='#00BCD4'
          containerViewStyle={styles.pairButton}
          borderRadius={50}
          raised
          title='Change password'
          onPress={() => navigate('ChangePassword')}
        />
        <StyledButton
          backgroundColor='#009688'
          containerViewStyle={styles.pairButton}
          borderRadius={50}
          raised
          title='Social links'
          onPress={this.getSocialLinks}
        />

        {avatarChanged ? <StyledButton
          backgroundColor='#009688'
          containerViewStyle={styles.pairButton}
          borderRadius={50}
          raised
          title='Upload avatar'
          onPress={this.uploadAvatar}
        /> : null}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    backgroundColor: '#fff',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  pairButton: {
    width: '50%',
    alignSelf: 'center',
    marginTop: 10
  },
  searchBarWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignContent: 'center',
    backgroundColor: '#fff',
    marginBottom: 5,
  },
  avatarContainer: {
    borderColor: '#9B9B9B',
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: 'center',
    alignItems: 'center'
  },
  avatar: {
    borderRadius: 75,
    width: 150,
    height: 150
  }
});