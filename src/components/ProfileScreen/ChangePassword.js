import React from 'react';
import axios from 'axios';
import { View, AsyncStorage } from 'react-native';
import { FormLabel, FormInput, FormValidationMessage, Button as StyledButton } from 'react-native-elements';
import { changePasswordValidation } from '../../helpers/helpers'
import { styles } from '../styles'


export default class ChangePassword extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      old_password: 'admin',
      new_password: 'dunice',
      retype: 'dunice',
      error: '',
    };
  }

  changePassword = async (check) => {
    const { goBack } = this.props.navigation;
    if (check === 'isValid') {
      const { old_password, new_password } = this.state;
      try {
        const token = await AsyncStorage.getItem('token');
        const response = await axios({
          method: 'patch',
          url: 'http://ec2-54-83-188-86.compute-1.amazonaws.com/profile/password/',
          headers: {
            'x-access-token': token,
          },
          data: {
            old_password: old_password,
            new_password: new_password,
          },
        })
        goBack();
      }
      catch (err) {
        console.log(err.message);
      }
    } else {
      this.setState((prevState) => {
        return { ...prevState, error: check };
      });
    }
  }

  saveNewPassword = () => {
    const isValid = changePasswordValidation.bind(this, this.state);
    this.changePassword(isValid());
  }

  render() {
    const { old_password, new_password, retype, error } = this.state;
    return (
      <View>
        <FormLabel>Current password</FormLabel>
        <FormInput
          value={ old_password }
          autoCapitalize='none'
          placeholder="Please enter your current password"
          keyboardType="default"
          secureTextEntry={true}
          onChangeText={value => this.setState({ old_password: value })}/>
        <FormValidationMessage>
          {error === null ? null : error.old_password || null}
        </FormValidationMessage>
        <FormLabel>New password</FormLabel>
        <FormInput
          value={ new_password }
          autoCapitalize='none'
          placeholder="Please enter your new password"
          keyboardType="default"
          secureTextEntry={true}
          onChangeText={value => this.setState({ new_password: value })}/>
        <FormValidationMessage>
          {error === null ? null : error.new_password || null}
        </FormValidationMessage>
        <FormLabel>Retype password</FormLabel>
        <FormInput
          value={ retype }
          autoCapitalize='none'
          placeholder="Please retype your new password"
          keyboardType="default"
          secureTextEntry={true}
          onChangeText={value => this.setState({ retype: value })}/>
        <FormValidationMessage>
          {error === null ? null : error.retype || null}
        </FormValidationMessage>


        <StyledButton
          backgroundColor='#CDDC39'
          containerViewStyle={{ width: '50%', alignSelf: 'center', marginBottom: 10 }}
          borderRadius={50}
          raised
          title='Save'
          onPress={this.saveNewPassword}
        />
        <StyledButton
          backgroundColor='#FF9800'
          containerViewStyle={styles.pairButton}
          borderRadius={50}
          raised
          title='Reset'
          onPress={() => this.setState((prevState) => {
            return {
              ...prevState, old_password: '', new_password: '', retype: '', error: '',
            };
          })}
        />
      </View>
    );
  }
}
