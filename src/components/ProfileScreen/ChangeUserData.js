import React from 'react';
import { View, StyleSheet, AsyncStorage } from 'react-native';
import {
  FormLabel,
  FormInput,
  FormValidationMessage,
  Button as StyledButton,
} from 'react-native-elements';
import axios from 'axios';

export default class ChangeUserData extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      first_name: this.props.first_name,
      last_name: this.props.last_name,
      bio: '',
      location: '',
      about: '',
      error: null,
    };
  }

  saveUserData = async () => {
    this.setState(prevState => {
      return { ...prevState, isTimePassed: false };
    })

    const {
      first_name,
      last_name,
      bio,
      location,
      about,
    } = this.state;
    try {
      const token = await AsyncStorage.getItem('token');
      const response = await axios({
        method: 'patch',
        url: 'http://ec2-54-83-188-86.compute-1.amazonaws.com/profile/update/',
        headers: {
          'x-access-token': token,
        },
        data: {
          first_name: first_name,
          last_name: last_name,
          bio: bio,
          location: location,
          about: about,
        },
      })
      const newState = {
        first_name: first_name,
        last_name: last_name,
      };
      this.props.changeCreds(newState);
    } catch (error) {
      if (error.response) {
        if (error.response.data.info === 'TOKEN_EXPIRED') {
          const refresh = await AsyncStorage.getItem('refresh');
          this.props.screenProps.refreshTokens(refresh, this.saveUserData);
        }
      } else if (error.request) {
        console.log(error.request);
      } else {
        console.log('Error', error.message);
      }
      console.log(error.config);
    }
  };

  render() {
    const {
      first_name,
      last_name,
      error,
    } = this.state;
    return (
      <View>
        <FormLabel>First name</FormLabel>
        <FormInput
          containerStyle={{ borderColor: 'red' }}
          value={ first_name }
          autoCapitalize='none'
          placeholder="Please enter your first name"
          keyboardType="default"
          onChangeText={value => this.setState(prevState => {
            return { ...prevState, first_name: value };
          })}/>
        <FormValidationMessage>
          {error === null ? null : error.first_name || null}
        </FormValidationMessage>

        <FormLabel>Last name</FormLabel>
        <FormInput
          value={last_name}
          autoCapitalize='none'
          placeholder="Please enter your last name"
          keyboardType="default"
          onChangeText={value => this.setState(prevState => {
            return { ...prevState, last_name: value };
          })}/>
        <FormValidationMessage>
          {error === null ? null : error.last_name || null}
        </FormValidationMessage>

        <StyledButton
          backgroundColor='#3F51B5'
          containerViewStyle={styles.pairButton}
          borderRadius={50}
          raised
          title='Save'
          onPress={this.saveUserData}
        />
        <StyledButton
          backgroundColor='#FF5722'
          containerViewStyle={styles.pairButton}
          borderRadius={50}
          raised
          title='Cancel'
          onPress={this.props.changeUserData}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    backgroundColor: '#fff',
  },
  pairButton: {
    width: '50%',
    alignSelf: 'center',
    marginTop: 10,
  },
  searchBarWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignContent: 'center',
    backgroundColor: '#fff',
    marginBottom: 5,
  },
});
