import { connect } from 'react-redux';
import { actions } from '../../store/authReducer';
import ProfileScreen from './ProfileScreen';

const MapDispatchToProps = {
  ...actions,
}

const MapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
  user: state.auth.user,
})

export default connect(MapStateToProps, MapDispatchToProps)(ProfileScreen);
