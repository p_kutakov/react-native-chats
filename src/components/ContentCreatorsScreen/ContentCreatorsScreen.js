import React from 'react';
import { View, Text, Platform, ScrollView } from 'react-native';
import { Avatar, List, ListItem, Icon } from 'react-native-elements';
import uuidv4 from 'uuid/v4';
import truncate from 'lodash/truncate';
import { styles } from '../styles';

export default class ContentCreatorsScreen extends React.Component {
  static navigationOptions = ({ navigation, screenProps }) => ({
    title: `${navigation.state.params.name}`,
    headerTintColor: '#515151',
    // headerLeft:<Icon raised={false} onPress={ () => { navigation.goBack() }} iconStyle={{marginLeft:20}} name='keyboard-backspace' />
  });
  renderIcon = () => {
    if (Platform.OS === 'ios') {
      return (
        <Icon
          iconStyle={styles.listTitleIcon}
          size={16}
          name='check-circle'
          color='#00aced'/>
      );
    }
    return (
      <Icon
        containerStyle={styles.listTitleIconAndroid}
        size={16}
        name='check-circle'
        color='#00aced'/>);
  }

  render() {
    const { params } = this.props.navigation.state;
    const list = [
      {
        name: 'Jack',
        subtitle: '70% match',
        icon: 'av-timer',
      },
      {
        name: 'VeryLongAwesomeNameForTest',
        subtitle: '50% match',
        icon: 'flight-takeoff',
      },
      {
        name: 'VeryLongAwesomeNameForTest',
        subtitle: '50% match',
        icon: 'flight-takeoff',
      },
      {
        name: 'VeryLongAwesomeNameForTest',
        subtitle: '50% match',
        icon: 'flight-takeoff',
      },
      {
        name: 'VeryLongAwesomeNameForTest',
        subtitle: '50% match',
        icon: 'flight-takeoff',
      },
      {
        name: 'VeryLongAwesomeNameForTest',
        subtitle: '50% match',
        icon: 'flight-takeoff',
      },
      {
        name: 'VeryLongAwesomeNameForTest',
        subtitle: '50% match',
        icon: 'flight-takeoff',
      },
    ]
    return (
      <ScrollView>
        <List containerStyle={styles.listContainer}>
          {
            list.map((item, i) => (
              <ListItem
                underlayColor='red'
                containerStyle={styles.listItemContainer}
                avatar={
                  <View style={styles.listAvatarWrapper}>
                    <Avatar
                      large
                      rounded
                      source={{ uri: "https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg" }}
                      onPress={() => console.log("Works!")}
                      activeOpacity={0.7}
                    />
                    <View style={styles.circle}/>
                  </View>
                }
                title={

                  <View style={styles.listItemTitleWrapper}>
                    <Text style={styles.listTitleUserName}>
                      {truncate(item.name, { 'length': 18, })}
                    </Text>
                    {this.renderIcon()}
                  </View>
                }
                key={uuidv4()}
                titleStyle={styles.listTitle}
                subtitle={item.subtitle}
                subtitleStyle={styles.listSubtitle}
                chevronColor={'#d0dcff'}
                rightTitle={`${i + 1}.`}
                rightTitleStyle={styles.listRightTitle}
                rightTitleContainerStyle={{ flex: 0.3 }}
              />
            ))}
        </List>
      </ScrollView>
    )
  }
}
