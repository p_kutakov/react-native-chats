import { StackNavigator, NavigationActions } from 'react-navigation';
import HomeScreen from './HomeScreen/HomeScreenContainer';
import RegistrationScreen from './HomeScreen/RegistrationScreen';
import PairFilterScreen from './PairFilterScreen/PairFilterScreen';
import ContentCreatorsScreen from './ContentCreatorsScreen/ContentCreatorsScreen';
import ProfileScreen from './ProfileScreen/ProfileScreenContainer';
import ChangePassword from './ProfileScreen/ChangePassword';
import ChatScreen from './ChatScreen/ChatScreen';

export const resetToHome = NavigationActions.reset({
  index: 0,
  actions: [
    NavigationActions.navigate({ routeName: 'Home' }),
  ],
})
export const resetToPairs = NavigationActions.reset({
  index: 0,
  actions: [
    NavigationActions.navigate({ routeName: 'PairFilter' }),
  ],
})
export const resetToProfile = NavigationActions.reset({
  index: 0,
  actions: [
    NavigationActions.navigate({ routeName: 'Profile' }),
  ],
})

const RootNavigator = StackNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: {
      gesturesEnabled: false,
      headerLeft: false,
    },
  },
  Registration: {
    screen: RegistrationScreen,
  },
  PairFilter: {
    screen: PairFilterScreen,
    navigationOptions: {
      gesturesEnabled: false,
      headerLeft: false,
      headerStyle: {
        height: 5,
      },
    },
  },
  ContentCreators: {
    screen: ContentCreatorsScreen,
    headerStyle: {
      height: 5,
    },
  },
  Profile: {
    screen: ProfileScreen,
    navigationOptions: {
      headerStyle: {
        height: 5,
      },
    },
  },
  ChangePassword: {
    screen: ChangePassword,
  },
  Chat: {
    screen: ChatScreen,
  },
});

export default RootNavigator;