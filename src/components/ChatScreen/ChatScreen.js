import React from 'react';
import { View, Text, Modal, TextInput, Image, StyleSheet } from 'react-native';
import { GiftedChat, Send, Composer, Bubble } from 'react-native-gifted-chat';
import { Button as StyledButton, Icon } from 'react-native-elements';
// import { styles } from '../styles';


export default class ChatScreen extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      visible: false,
      isActive: false,
    };
  }

  componentWillMount() {
    this.setState({
      messages: [
        {
          _id: 1,
          text: 'Hello developer',
          createdAt: new Date(),
          user: {
            _id: 2,
            name: 'React Native',
            avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
          },
        },
      ],
    });
  }

  onSend(messages = []) {
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, messages),
    }));
  }

  onPress = () => {
    this.setState((prevState) => {
      return { ...prevState, visible: !this.state.visible };
    });
  }

  renderFooter = (props) => {
    return (
      <View style={{
        flexDirection: 'row',
        flex: 2,
        justifyContent: 'center',
        alignContent: 'center',
        paddingLeft: 10,
        paddingRight: 10,
      }}>
        <Icon
          containerStyle={{ justifyContent: 'center', alignContent: 'center', margin: 0 }}
          raised
          name='pan-tool'
          type='material'
          color='#f50'
          size={18}
          onPress={this.onPress}/>
        <Composer {...props} placeholder={'My placeholder'}/>

        {this.renderSendButton(props)}
      </View>);
  }

  renderSendButton = (props) => {
    return (
      <Send
        {...props}
        containerStyle={{ justifyContent: 'center' }}
      >
        <Icon
          name='send'/>
      </Send>
    );
  }

  renderBubble = (props) => {
    return (
      <Bubble {...props}
              wrapperStyle={{
                left: {
                  backgroundColor: '#FF9966',
                },
                right: {
                  backgroundColor: '#219BE8',
                },
              }}/>
    );
  };

  render() {
    return (
      <View style={{
        flex: 1,
        alignContent: 'center',
        justifyContent: 'flex-end',
        backgroundColor: '#fff',
      }}>
        <Modal presentationStyle='overFullScreen' animationType='fade' transparent={true}
               visible={this.state.visible}>
          <View
            style={{
              flex: 1,
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View style={{
              width: '60%',
              backgroundColor: '#E0E0E0',
              borderWidth: 1,
              borderColor: '#000',
              borderRadius: 5,
              padding: 15,
            }}>
              <View>
                <Text style={{ textAlign: 'center' }}>
                  OFFER
                </Text>
                <Text style={{ textAlign: 'center', marginBottom: 15 }}>
                  00.00.0000
                </Text>
                <Text style={{ textAlign: 'center', marginBottom: 15 }}>
                  Very long description of deal.................................
                  ...........................................................
                  ...........................................................
                </Text>
              </View>

              <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                <StyledButton
                  backgroundColor='#4CAF50'
                  containerViewStyle={styles.pairButton}
                  borderRadius={50}
                  raised
                  title='Confirm'
                  onPress={this.onPress}
                />
                <StyledButton
                  backgroundColor='#f44336'
                  containerViewStyle={styles.pairButton}
                  borderRadius={50}
                  raised
                  title='Decline'
                  onPress={ this.onPress }
                />
              </View>
            </View>
          </View>
        </Modal>
        <GiftedChat

          bottomOffset={50}
          messages={this.state.messages}
          onSend={messages => this.onSend(messages)}
          user={{
            _id: 1,
          }}
          renderInputToolbar={this.renderFooter}
          renderBubble={this.renderBubble}
        />
      </View>
    );
  }
}

const
  styles = StyleSheet.create({
    footerContainer: {
      marginTop: 5,
      marginLeft: 10,
      marginRight: 10,
      marginBottom: 10,
    },
    footerText: {
      fontSize: 14,
      color: '#aaa',
    },
  });
