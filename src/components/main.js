import React from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import Drawer from 'react-native-drawer';
import { connect } from 'react-redux';
import { View, AsyncStorage, ActivityIndicator } from 'react-native';
import DrawlerContent from './DrawlerContent';
import RootNavigator, { resetToHome, resetToPairs, resetToProfile } from './navigation';
import { changeAuthStatus } from '../store/authReducer';
import { styles } from './styles';


const MapDispatchToProps = {
  changeAuthStatus,
}

const MapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
  user: state.auth.user,
})

export class MainComponent extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      drawer: false,
      isTimePassed: false,
    };
  }

  componentDidMount() {
    this.getItem();
  }

  getItem = async () => {
    try {
      const value = await AsyncStorage.getItem('token');
      if (value !== null) {
        this.props.changeAuthStatus(true);
        this.setState((prevState) => {
          return { ...prevState, isTimePassed: true };
        })

        this.navigator.dispatch(resetToPairs);
      } else {
        this.setState((prevState) => {
          return { ...prevState, isTimePassed: true };
        })
        this.navigator.dispatch(resetToHome);
      }
    } catch (error) {
      this.props.changeAuthStatus(false)
      this.setState((prevState) => {
        return { ...prevState, isTimePassed: true };
      })
      this.navigator.dispatch(resetToHome);
    }
  }

  refreshTokens = async (refresh, request) => {
    try {
      const response = await axios({
        method: 'get',
        url: 'http://ec2-54-83-188-86.compute-1.amazonaws.com/auth/refresh/',
        headers: {
          'x-refresh-token': refresh,
        },
      })
      const { token, refresh_token } = response.data;
      await AsyncStorage.setItem('token', token);
      await AsyncStorage.setItem('refresh', refresh_token);
      request();
    } catch (error) {
      if (error.response) {
        if (error.response.data.info === 'REFRESH_TOKEN_EXPIRED') {
          this.actionDrawler(false);
          this.props.changeAuthStatus(false);
          this.navigator.dispatch(resetToHome);
        }
      } else if (error.request) {
        console.log(error.request);
      } else {
        console.log('Error', error.message);
      }
      console.log(error.config);
    }
  }

  onLogout = async () => {
    const token = await AsyncStorage.getItem('token');
    const refresh = await AsyncStorage.getItem('refresh');
    try {
      const response = await axios({
        method: 'get',
        url: 'http://ec2-54-83-188-86.compute-1.amazonaws.com/auth/logout/',
        headers: {
          'x-access-token': token,
          'x-refresh-token': refresh,
        },
      });
      await this.removeToken();
      if (+response.data.status === 200) {
        this.actionDrawler(false);
        this.props.changeAuthStatus(false);
        this.navigator.dispatch(resetToHome);
      }
    } catch (error) {
      if (error.response) {
        if (error.response.data.info === "TOKEN_EXPIRED") {
          this.refreshTokens(refresh, this.onLogout);
        }
      } else if (error.request) {
        console.log(error.request);
      } else {
        console.log('Error', error.message);
      }
      console.log(error.config);
    }
  }

  removeToken = async () => {
    try {
      await AsyncStorage.removeItem('token');
      await AsyncStorage.removeItem('refresh');
    } catch (err) {
      console.log(err);
    }
  }

  actionDrawler = (value) => {
    if (value) {
      this._drawler.open();
    } else {
      this._drawler.close();
    }
  }

  goToPairs = () => {
    this.navigator.dispatch(resetToPairs);
    this.actionDrawler();
  }

  goToProfile = () => {
    this.navigator.dispatch(resetToProfile);
    this.actionDrawler();
  }

  render() {
    const { isTimePassed } = this.state;
    const screenProps = {
      actionDrawler: this.actionDrawler,
      refreshTokens: this.refreshTokens,
    }
    if (isTimePassed === false) {
      return (
        <View style={styles.container}>
          <ActivityIndicator
            size="large"
            color='#040404'
          />
        </View>
      );
    }
    return (
      <Drawer
        type="overlay"
        content={<DrawlerContent
          onLogout={this.onLogout}
          goToProfile={this.goToProfile}
          goToPairs={this.goToPairs}

        />}
        openDrawerOffset={0.6}
        closedDrawerOffset={0}
        tapToClose={true}
        onCloseStart={this.openDrawler}
        ref={(ref) => this._drawler = ref}
      >
        <RootNavigator
          ref={ref => {
            this.navigator = ref;
          }}
          initialRouteName="PairFilterScreen"
          screenProps={screenProps}/>
      </Drawer>
    );
  }
}


MainComponent.propTypes = {
  changeAuthStatus: PropTypes.func,
};

export default connect(MapStateToProps, MapDispatchToProps)(MainComponent);
