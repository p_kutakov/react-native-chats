import React from 'react';
import { View } from 'react-native';
import { Button as StyledButton } from 'react-native-elements';


export default class DrawlerContent extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={{ flex: 1, paddingTop: 20, backgroundColor: '#fafafa' }}>
        <StyledButton
          raised
          backgroundColor='#2196F3'
          containerViewStyle={{ marginBottom: 10 }}
          title='Pairs'
          onPress={this.props.goToPairs}
        />
        <StyledButton
          raised
          backgroundColor='#2196F3'
          containerViewStyle={{ marginBottom: 10 }}
          title='Profile'
          onPress={this.props.goToProfile}
        />
        <StyledButton
          raised
          backgroundColor='#2196F3'
          title='LOGOUT'
          onPress={this.props.onLogout}
        />
      </View>
    );
  }
}
