import { connect } from 'react-redux';
import { actions } from '../../store/authReducer';
import HomeScreen from './HomeScreen';

const MapDispatchToProps = {
  ...actions,
}

const MapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
  user: state.auth.user,
})

export default connect(MapStateToProps, MapDispatchToProps)(HomeScreen);
