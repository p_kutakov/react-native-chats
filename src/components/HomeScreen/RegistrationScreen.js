import React from 'react';
import { View, Text } from 'react-native';
import axios from 'axios';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { FormLabel, FormInput, FormValidationMessage, Button as StyledButton } from 'react-native-elements';
import { styles } from '../styles';
import { sugnUpValidation } from '../../helpers/helpers';

export default class RegistrationScreen extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      first_name: '',
      last_name: '',
      password: '',
      confirmation: '',
      email: '',
      account_type: this.props.navigation.state.params.account_type,
      error: null,
      isLoading: false,
      success: false
    }
  }

  componentDidMount() {
    this.setState({
      error: null,
    });
  }


  validationCheck = async (check) => {
    if (check === 'isValid') {
      const {first_name, last_name, password, confirmation, email, account_type} = this.state;
      try {
        const response = await axios({
          method: 'post',
          url: 'http://ec2-54-83-188-86.compute-1.amazonaws.com/auth/register',
          data: {
            first_name: first_name,
            last_name: last_name,
            password: password,
            confirmation: confirmation,
            email: email,
            account_type: 'influencer'
          }
        })

        this.setState((prevState) => {
          return {...prevState, isLoading: false, success: true};
        });
      }
      catch (err) {
        console.log(err)
      }
    }
    else {
      this.setState((prevState) => {
        return {...prevState, error: check, isLoading: false};
      });
    }
  }
  SignUp = () => {
    this.setState((prevState) => {
      return {...prevState, isLoading: true};
    });
    const isValid = sugnUpValidation.bind(this, this.state)
    this.validationCheck(isValid())
  }

  render() {
    const {navigate} = this.props.navigation;
    const {first_name, last_name, password, confirmation, email, error, isLoading} = this.state;
    if (this.state.success) {
      return (
        <View style={styles.container}>
          <Text style={{marginBottom: 15, color: '#4CAF50', fontWeight: '500'}}>
            Registration successful!!!
          </Text>

          <StyledButton
            disabled={isLoading}
            raised
            leftIcon={{name: 'arrow-back'}}
            title='Proceed to login'
            onPress={() => navigate('Home', {name: 'HomeScreen'})}
            backgroundColor="#4CAF50"
          />
        </View>
      )
    }
    return (
      <KeyboardAwareScrollView
        style={styles.insideContainerWrapperIOS}
        extraHeight={250}
        enableOnAndroid={true}
      >
        <FormLabel>First name</FormLabel>
        <FormInput
          value={first_name}
          autoCapitalize='none'
          placeholder="Please enter your name"
          keyboardType="default"
          onChangeText={value => this.setState({first_name: value})}/>
        <FormValidationMessage>{error === null ? null : error.first_name || null}</FormValidationMessage>

        <FormLabel>Last name</FormLabel>
        <FormInput
          value={last_name}
          autoCapitalize='none'
          placeholder="Please enter your last name"
          keyboardType="default"
          onChangeText={value => this.setState({last_name: value})}/>
        <FormValidationMessage>{error === null ? null : error.last_name || null}</FormValidationMessage>

        <FormLabel>Email</FormLabel>
        <FormInput
          value={email}
          autoCapitalize='none'
          placeholder="Please enter your email"
          keyboardType="email-address"
          onChangeText={value => this.setState({email: value})}/>
        <FormValidationMessage>{error === null ? null : error.email || null}</FormValidationMessage>

        <FormLabel>Password</FormLabel>
        <FormInput
          value={password}
          autoCapitalize='none'
          placeholder="Please enter your password"
          keyboardType="default"
          secureTextEntry={true}
          onChangeText={value => this.setState({password: value})}/>
        <FormValidationMessage>{error === null ? null : error.password || null}</FormValidationMessage>

        <FormLabel>Confirmation</FormLabel>
        <FormInput
          value={confirmation}
          autoCapitalize='none'
          placeholder="Please confirm your password"
          keyboardType="default"
          secureTextEntry={true}
          onChangeText={value => this.setState({confirmation: value})}/>
        <FormValidationMessage>{error === null ? null : error.confirmation || null}</FormValidationMessage>
        <StyledButton
          disabled={isLoading}
          raised
          rightIcon={{name: 'forward'}}
          title='SIGN UP'
          onPress={this.SignUp}
          backgroundColor="#468464"
        />
      </KeyboardAwareScrollView>
    )
  }
}