import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import { View, AsyncStorage } from 'react-native';
import { FormLabel, FormInput, FormValidationMessage, Button as StyledButton } from 'react-native-elements';
import { styles } from '../styles';
import { loginValidation } from '../../helpers/helpers';
import { resetToPairs } from '../navigation';
export default class LoginForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      email: 'p.kutakov@dunice.com',
      password: 'dunice',
      error: null,
      isLoading: false,
    };
  }

  sendLoginRequest = async (check) => {
    const { dispatch } = this.props.navigation;
    const { email, password } = this.state;
    if (check === 'isValid') {
      try {
        const response = await axios.post('http://ec2-54-83-188-86.compute-1.amazonaws.com/auth/login', {
          password: password,
          email: email,
        })
        const { token, refresh_token } = response.data;
        const user = response.data.data;
        await AsyncStorage.setItem('token', token);
        await AsyncStorage.setItem('refresh', refresh_token);
        this.props.saveUser(user);
        this.setState((prevState) => {
          return {
            ...prevState, isLoading: false,
          };
        });
        dispatch(resetToPairs);
      } catch (error) {
        console.log(error)
        this.setState((prevState) => {
          return {
            ...prevState, error: check, isLoading: false,
          };
        });
      }
    } else {
      this.setState((prevState) => {
        return {
          ...prevState, error: check, isLoading: false,
        };
      });
    }
  }

  onLogin = () => {
    this.setState((prevState) => {
      return {
        ...prevState, isLoading: true,
      };
    });
    const isValid = loginValidation.bind(this, this.state)
    this.sendLoginRequest(isValid());
  }

  render() {
    const { password, email, error, isLoading } = this.state;
    return (
      <View style={styles.loginForm}>
        <FormLabel>Email</FormLabel>
        <FormInput
          value={email}
          autoCapitalize='none'
          placeholder="Please enter your email"
          keyboardType="email-address"
          onChangeText={value => this.setState((prevState) => {
            return { ...prevState, email: value };
          })}/>
        <FormValidationMessage>
          {error === null ? null : error.email }
        </FormValidationMessage>

        <FormLabel>Password</FormLabel>
        <FormInput
          value={password}
          autoCapitalize='none'
          placeholder="Please enter your password"
          keyboardType="default"
          secureTextEntry={true}
          onChangeText={value => this.setState({ password: value })}/>
        <FormValidationMessage>
          {error === null ? null : error.password || null}
        </FormValidationMessage>
        <StyledButton
          disabled={isLoading}
          raised
          rightIcon={{ name: 'forward' }}
          title='LOGIN'
          onPress={this.onLogin}
          backgroundColor="#468464"
        />
      </View>
    );
  }
}

LoginForm.propTypes = {
  getAuth: PropTypes.func,
  saveUser: PropTypes.func,
};
