import React from 'react';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { View, Platform } from 'react-native';
import { Button as StyledButton } from 'react-native-elements';
import LoginForm from './LoginForm';
import { styles } from '../styles';


export default class HomeScreen extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      showLogin: false,
      isTimePassed: false,
    };
  }

  getAuth = () => {
    this.setState((prevState) => {
      return {
        ...prevState, showLogin: false,
      };
    });
  }

  showLogin = () => {
    if (this.state.showLogin) {
      return (
        <LoginForm
          getAuth={this.getAuth}
          navigation={this.props.navigation}
          saveUser={this.props.saveUser}
        />
      );
    }
  }
  showAuthButtons = () => {
    const { showLogin } = this.state;
    return (
      <StyledButton
        onPress={() => this.setState({ showLogin: !showLogin })}
        title={showLogin ? 'close' : 'login'}
        backgroundColor="#468464"
        borderRadius={10}
      />
    );
  }

  render() {
    const { navigate } = this.props.navigation;
    const keyboardAvoidIOS = {
      style: styles.insideContainerWrapperIOS,
      extraHeight: 200,
      contentContainerStyle: styles.main,
    }
    const keyboardAvoidAndroid = {
      enableOnAndroid: true,
      extraHeight: 200,
      contentContainerStyle: styles.insideContainer,
      style: styles.insideContainerWrapper,
      scrollEnabled: true,
    }
    const AwareScrollProps = Platform.OS === 'ios' ? keyboardAvoidIOS : keyboardAvoidAndroid;
    return (
      <KeyboardAwareScrollView
        {...AwareScrollProps}
      >
        <View style={styles.blockAbove}>
          <StyledButton
            onPress={() =>
              navigate('Registration', { name: 'Registration', account_type: 'influencer' })
            }
            title='influencer'
            backgroundColor="#484658"
            borderRadius={10}
          />
          <StyledButton
            onPress={() =>
              navigate('Registration', { name: 'Registration', account_type: 'company' })
            }
            title='company'
            backgroundColor="#841584"
            borderRadius={10}
          />

        </View>
        <View style={styles.blockBelow}>
          {this.showAuthButtons()}
          {this.showLogin()}
        </View>
      </KeyboardAwareScrollView>
    );
  }
}

