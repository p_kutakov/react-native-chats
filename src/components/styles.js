import { StyleSheet, Dimensions } from 'react-native'
const white = '#fff';
const cream = '#fafafa';
const red = '#dd2323';
const online = '#8dd73f';
const black = '#040404';
const coffee = '#515151';


const { height } = Dimensions.get('window');
const textAreaHeight = height > 600 ? height / 4 : height / 5;
const multiSliderHeight = height > 600 ? 30 : 20;
const smallTitleFontSize = height > 600 ? 18 : 16;


export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: cream,
  },
  main: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  insideContainerWrapper: {
    backgroundColor: '#fafafa'
  },
  insideContainerWrapperIOS: {
    flex: 1,
    backgroundColor: "#fafafa"
  },
  insideContainer: {
    flexBasis: 500,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  blockAbove: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10
  },
  blockBelow: {
    flex: 0.2,
    flexDirection: 'column',
    alignItems: 'center',
  },
  loginForm: {
    width: '90%',
    alignItems: 'stretch',
    alignContent: 'center',
  },
  input: {
    height: 40,
    color: 'black'
  },
  inputError: {
    height: 40,
    color: 'red'
  },
  smallTitle: {
    fontSize: smallTitleFontSize,
    fontWeight: 'bold',
    textAlign: 'center',
    color: red,
    marginBottom: 5
  },
  searchBarWrapper: {
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: white,
    marginBottom: 5
  },
  searchBarIcon: {
    flex: 0.1,
    marginLeft: '2%',
    marginRight: '2%',
    backgroundColor: white
  },
  textAreaContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: '2%',
    marginBottom: '3%',
    flexShrink: 2,

  },
  textArea: {
    height: textAreaHeight,
    textAlign: 'center',
    width: '90%',
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 10,

  },
  sliderBlockContainer: {
    width: '90%',
    alignSelf: 'center'
  },
  sliderValuesWrapper: {
    width: '80%',
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'baseline',
    marginBottom: 10
  },
  multiSliderWrapper: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  multiSlider: {
    height: multiSliderHeight
  },
  circle: {
    width: 10,
    height: 10,
    borderRadius: 10 / 2,
    position: 'absolute',
    bottom: 5,
    right: 5,
    backgroundColor: online
  },
  socialIconsContainer: {
    alignSelf: 'center',
    width: '90%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  searchBarContainer: {
    flex: 1,
    backgroundColor: white,
    borderBottomColor: cream,
    borderTopColor: cream
  },
  searchBarInput: {
    backgroundColor: cream,
    borderColor: cream
  },
  listContainer: {
    borderTopColor: white,
    marginTop: 0
  },
  listItemContainer: {
    backgroundColor: white
  },
  listAvatarWrapper: {
    position: 'relative'
  },
  listItemTitleWrapper: {
    paddingLeft: 10,
    alignSelf: 'flex-start',
    position: 'relative'
  },
  listTitleUserName: {
    fontSize: 14,
    color: coffee
  },
  listTitleIcon: {
    position: 'absolute',
    top: -30,
    right: -20
  },
  listTitleIconAndroid: {
    position: 'absolute',
    top: -10,
    right: -20
  },
  listSubtitle: {
    color: red
  },
  listRightTitle: {
    color: black,
    marginRight: 10
  },
  listTitle: {
    color: coffee
  },
  pairButton: {
    width: '50%',
    alignSelf: 'center',
  },
  spacer: {
    height: 30
  }

})


export default styles