//function that responsable for validation in the submit forms actions
export function sugnUpValidation(state) {
  const errors = {}
  if (!state.first_name) {
    errors.first_name = 'Required';
  } else if (state.first_name.length > 15) {
    errors.first_name = 'Must be 15 characters or less';
  }
  if (!state.last_name) {
    errors.last_name = 'Required';
  } else if (state.last_name.length > 15) {
    errors.last_name = 'Must be 15 characters or less';
  }
  if (!state.email.length) {
    errors.email = 'Required'
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(state.email)) {
    errors.email = 'Invalid email address';
  }
  if (!state.password) {
    errors.password = 'Required';
  } else if (state.password.length < 5) {
    errors.password = 'Must be 6 characters or more';
  }
  if (!state.confirmation) {
    errors.confirmation = 'Required';
  } else if (state.confirmation.length < 5) {
    errors.confirmation = 'Must be 6 characters or more';
  }
  if (state.confirmation !== state.password) {
    errors.confirmation = 'Passwords are should be equal';
  }
  if (Object.keys(errors).length) {
    return errors;
  } else {
    this.setState({ error: null })
    return 'isValid';
  }
}

export function loginValidation(state) {
  const errors = {}
  console.log('state-- ', state);
  if (!state.email.length) {
    errors.email = 'Required';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(state.email)) {
    errors.email = 'Invalid email address';
  }
  if (!state.password) {
    errors.password = 'Required';
  } else if (state.password.length < 5) {
    errors.password = 'Must be 6 characters or more';
  }
  if (Object.keys(errors).length > 0) {
    return errors;
  } else {
    return 'isValid';
  }
}

export function changePasswordValidation(state) {
  const errors = {}
  console.log('state-- ', state);
  if (!state.old_password) {
    errors.old_password = 'Required';
  } else if (state.old_password.length < 5) {
    errors.old_password = 'Must be 6 characters or more';
  }
  if (!state.new_password) {
    errors.new_password = 'Required';
  } else if (state.new_password.length < 5) {
    errors.new_password = 'Must be 6 characters or more';
  }
  if (!state.retype) {
    errors.retype = 'Required';
  } else if (state.retype.length < 5) {
    errors.retype = 'Must be 6 characters or more';
  }
  if (state.retype !== state.new_password) {
    errors.retype = 'Passwords are should be equal';
  }
  if (Object.keys(errors).length > 0) {
    return errors;
  } else {
    return 'isValid';
  }
}


