import { combineReducers } from 'redux';
import authReducer from './authReducer';


const makeRootReducer = () => {
  return combineReducers({
    auth: authReducer,

  });
}


export default makeRootReducer;
