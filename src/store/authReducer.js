export const CHANGE_AUTH_STATUS = 'CHANGE_AUTH_STATUS';
export const SAVE_USER = 'SAVE_USER';

export const changeAuthStatus = (value) => {
  return {
    type: CHANGE_AUTH_STATUS,
    payload: value,
  };
};

export const saveUser = (user) => {
  console.log(user)
  return {
    type: SAVE_USER,
    payload: user,
  };
};

export const actions = {
  changeAuthStatus,
  saveUser,
}

const ACTION_HANDLERS = {
  [CHANGE_AUTH_STATUS]: (state, action) => Object.assign({}, state, { isAuthenticated: action.payload }),
  [SAVE_USER]: (state, action) => Object.assign({}, state, { user: action.payload }),
}

const initialState = {
  isAuthenticated: false,
  user: {},
};
export default function authReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
}
