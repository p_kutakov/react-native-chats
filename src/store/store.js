import { createStore as CreateReduxStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import makeRootReducer from './rootReducer';


export const createStore = (initialState = {}) => {

  const middleware = [thunk]

  const enhancers = []
  const composeEnhancers = compose;

  const store = CreateReduxStore(
    makeRootReducer(),
    initialState,
    composeEnhancers(
      applyMiddleware(...middleware),
      ...enhancers,
    ),
  )

  return store;
};


export default createStore();
